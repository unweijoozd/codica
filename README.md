# **Codica test task for DevOps**

## **Task**
Create a template for AWS and a few extra things to speed up the whole process.

**Write with Terraform :**

- VPC (CIDR 10.0.0.0/16) with subnets (2 private and 2 public, CIDR doesn’t matter here)
- NAT and Internet gateways
- Application load balancer and target group
- RDS database db.t3.micro with custom subnet group and EC2 instance t3.micro(latest ubuntu AMI)
- Security groups, route tables, and other required resources
- Follow naming conventions from [here](https://www.terraform-best-practices.com/).
- Try to keep infra as secure as possible, and do not use any custom modules here.
- The state should be in a private S3 bucket

**Ansible:**

The simple playbook that will install docker, docker-compose v2, and required dependencies
Make it work with both RHEL and Debian-like operating systems

**Docker:**

Launch this [template](https://github.com/docker/awesome-compose/tree/master/wordpress-mysql) but instead of using the database defined in compose use your RDS instance

## How to start
**1. Clone the repository:**

`git clone https://gitlab.com/unweijoozd/codica.git`

**2. Create in AWS your key pair and add to project change variables for you key pair**

`variable "ssh_key_path" - "key.pem"`

`variable "key_name" - "key.pem"`

**3. Create in AWS private s3 bucket**

[Using the S3 console](https://docs.aws.amazon.com/AmazonS3/latest/userguide/create-bucket-overview.html)


**4. Log in the terminal to your AWS : [aws configure](https://docs.aws.amazon.com/cli/latest/reference/configure/):**

`aws configure --profile codica`

**6. To initialize the Terraform modules and see preview the actions Terraform would take to modify your infrastructure**

`terraform init, terraform plan`

**8. The terraform apply command executes the actions proposed in a terraform plan .**

`terraform apply`

**10.  To destroy terminates resources managed by your Terraform project.**

`terraform destroy`

### **Finally you can check Wordpress image page**

`http://<public_ip from output.tf>`
