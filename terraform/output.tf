output "public_ip" {
  value = aws_instance.ec2.public_ip
}

output "db_instance" {
   value = aws_db_instance.db_instance.endpoint
}