variable "profile" {
  default     = "shadow"
  description = "Codica project"
}

variable "environment" {
  default = "main"
}

variable "region" {
  default = "us-east-1"
}

variable "instance_type" {
  default = "t3.micro"
}

variable "ssh_user" {
  default = "ubuntu"
}

variable "key_name" {
  default = "my-n.virginia"
}

variable "ssh_key_path" {
  default = "my-n.virginia.pem"
}

variable "tag" {
  default = "Codica-TT"
}
