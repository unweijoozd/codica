#Creating EC2 instance
resource "aws_instance" "ec2" {

  ami                         = "ami-0557a15b87f6559cf" # Canonical, Ubuntu, 22.04 LTS, amd64 jammy image build on 2023-02-08
  instance_type               = var.instance_type
  key_name                    = var.key_name
  monitoring                  = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = aws_subnet.public_sub_1.id
  associate_public_ip_address = true

  tags = {
    Name = "${var.tag}-ec2"
  }
}

#Creating RDS instance
resource "aws_db_instance" "db_instance" {
  identifier     = "db-instance"
  instance_class = "db.t3.micro"
  engine         = "mysql"
  engine_version = "5.7"

  allocated_storage = 20
  db_name           = "codicaDB"
  username          = "codica"
  password          = "password"

  db_subnet_group_name   = aws_db_subnet_group.db_subnet_group.id
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
  publicly_accessible    = false
  skip_final_snapshot    = true

  tags = {
    Name = "${var.tag}-db-instance"
  }
}

resource "null_resource" "ansible" {
  provisioner "local-exec" {
    working_dir = "../ansible"
    command = "ansible-playbook --inventory ${aws_instance.ec2.public_ip}, --private-key ${var.ssh_key_path} --user=ubuntu playbook.yml"
  }
  depends_on = [aws_instance.ec2]
}
