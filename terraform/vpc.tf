# Create VPC
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "${var.tag}-vpc"
  }
}

# Create public subnets
resource "aws_subnet" "public_sub_1" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "${var.region}a"

  tags = {
    Name = "${var.tag}-public-sn-1"
  }
}

resource "aws_subnet" "public_sub_2" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = "${var.region}b"

  tags = {
    Name = "${var.tag}-public-sn-2"
  }
}

# Create private subnets
resource "aws_subnet" "private_sub_1" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "${var.region}a"

  tags = {
    Name = "${var.tag}-private-sn-1"
  }
}

resource "aws_subnet" "private_sub_2" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.4.0/24"
  availability_zone = "${var.region}b"

  tags = {
    Name = "${var.tag}-private-sn-2"
  }
}
