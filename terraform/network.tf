#Security Group
resource "aws_security_group" "sg" {
  name        = "sg"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "TCP"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "TCP"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.tag}-sg"
  }
}

# Security group for RDS instance
resource "aws_security_group" "rds_sg" {
  name        = "rds_sg"
  description = "Security Group RDS"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description     = "mysql"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.sg.id}"]
  }

  tags = {
    Name = "${var.tag}-rds-sg"
  }
}

# Create internet gateway and attach to VPC
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.tag}-igw"
  }
}

# Create NAT gateway and allocate Elastic IP
resource "aws_eip" "nat_eip" {
  vpc = true

  tags = {
    Name = "${var.tag}-nat-eip"
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_sub_1.id

  tags = {
    Name = "${var.tag}-nat-gw"
  }
}

# Create route tables
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "public-rt"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway.id
  }
  tags = {
    Name = "private-rt"
  }
}

# Associate subnets with route tables
resource "aws_route_table_association" "public_subnet_1_association" {
  subnet_id      = aws_subnet.public_sub_1.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "public_subnet_2_association" {
  subnet_id      = aws_subnet.public_sub_2.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private_subnet_1_association" {
  subnet_id      = aws_subnet.private_sub_1.id
  route_table_id = aws_route_table.private_route_table.id
}

resource "aws_route_table_association" "private_subnet_2_association" {
  subnet_id      = aws_subnet.private_sub_2.id
  route_table_id = aws_route_table.private_route_table.id
}

# Application load balancer and target group
resource "aws_lb" "lb" {
  name               = "lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg.id]
  subnets            = [aws_subnet.public_sub_1.id, aws_subnet.public_sub_2.id]

  tags = {
    Name = "${var.tag}-lb"
  }
}

resource "aws_lb_target_group" "lb-tg" {
  name_prefix        = "lb-tg"
  port               = 80
  protocol           = "HTTP"
  vpc_id             = aws_vpc.vpc.id
  target_type        = "ip"

  health_check {
    interval            = 30
    path                = "/"
    protocol            = "HTTP"
    timeout             = 10
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  tags = {
    Name = "${var.tag}-lb-tg"
  }
}

# Define RDS database in private subnet
resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "db_subnet_group"
  subnet_ids = [aws_subnet.private_sub_1.id, aws_subnet.private_sub_2.id]

  tags = {
    Name = "${var.tag}-db-sg"
  }
}

